-- While working on this module you are encouraged to remove it and fix
-- any warnings in the module. See
--     http://hackage.haskell.org/trac/ghc/wiki/WorkingConventions#Warnings
-- for details  

-----------------------------------------------------------------------------
-- |
-- Module      :  Vector
-- Copyright   :  (c) Lennart Schmitt
-- License     :  BSD-style (see the file libraries/base/LICENSE)
-- 
-- Maintainer  :  lennart...schmitt@<nospam>gmail.com
-- Stability   :  experimental
-- Portability :  portable
--
-- This module implements a few extensions for the vector-module.
-----------------------------------------------------------------------------

module Numeric.Vector (
           -- * Data Types
           RawVector,
           Vector, 
 
           -- * Functions
           average,
           count,
           fromList,-- | Convertes the representation from a simple list to a vector
           maximumBy, -- | Calculates a lists maximum depending on a given ordering-function
           maxPos,
           toList,  -- | Convertes the representation from a vector to a simple list
           transpose -- | Transposes a Vector
         ) where

import Numeric.LinearAlgebra (Vector, toList, fromList)
import Data.List (transpose, maximumBy)

-- | A Vector represented by a simple list.
type RawVector a = [a]

-- | Calculates the lists elements average
--
-- > average [1,3,2] == 2.0
average :: Floating a => RawVector a -> a
average xs = (sum xs) / (count xs)

-- | Calculates the position of a lists maximum
--
-- > maxPos [1,10,8,3] == 1 
maxPos :: RawVector Double -> Int
maxPos xs = fst $ foldl1 f [(i,x)|(i,x)<- zip [0..] xs]
            where 
            f a b = if snd a > snd b then a else b

-- | Counts the elements of a given list
--
-- > count [1,2,3,4,5] == 5
count :: Num b => RawVector a -> b
count = sum  .  map (const 1)
