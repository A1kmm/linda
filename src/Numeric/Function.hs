-- While working on this module you are encouraged to remove it and fix
-- any warnings in the module. See
--     http://hackage.haskell.org/trac/ghc/wiki/WorkingConventions#Warnings
-- for details  

-----------------------------------------------------------------------------
-- |
-- Module      :  Function
-- Copyright   :  (c) Lennart Schmitt
-- License     :  BSD-style (see the file libraries/base/LICENSE)
-- 
-- Maintainer  :  lennart...schmitt@<nospam>gmail.com
-- Stability   :  stable
-- Portability :  portable
--
-- This module implements the simple functionality of multidimensional linear function calculation.
-----------------------------------------------------------------------------

module Numeric.Function (
          -- * Data Types
          LinFunction,
          Values,          
          -- * Functions
          calcLinFunction
          )where

-- | The function-type represents a function by its constants, e.g.
-- 
-- > [b0,b1,...,bn] 
-- 
-- represents the function f = b0 + b1 * X1 + ... + bn * Xn
type LinFunction a = [a] 

-- | Similare to the function-type, but die value-type represents the values of the variables in a function, e.g.
-- 
-- > [X1,...,Xn]
type Values a = [a] 

-- | Calculates the result of a given function with given values, e.g.
-- 
-- > calcLinFunction [1,1,1] [1,2] == 1 + 1 * 1 + 1 * 2 == 4
--
-- > calcLinFunction [1,2,3] [1,1] == 1 + 2 * 1 + 3 * 1 == 6
--
-- > calcLinFunction [1,2,3] [1..] == 1 + 2 * 1 + 3 * 2 == 9
calcLinFunction :: Num a => LinFunction a -> Values a -> a
calcLinFunction f = sum . (zipWith (*) f ) . ([1]++)
